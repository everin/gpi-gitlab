# gpi-reveal.js

Template Reveal JS gruppo GPI

Presentazione: GPI GitLab

### Setup

Come installare il template:

1. Clonare il repository reveal.js
   ```sh
   $ git clone https://github.com/hakimel/reveal.js.git
   ```

1. Entrare nella directory reveal.js
   ```sh
   $ cd reveal.js
   ```

1. Installare le dipendenze per reveal.js
   ```sh
   $ npm install
   ```

1. Creare la directory "gpi-gitlab" e clonare il repository gpi-gitlab
   ```sh
   $ git clone https://gitlab.com/everin/gpi-gitlab.git
   ```
   
1. Attivare la pubblicazione delle presentazioni
   ```sh
   $ npm start
   ```

1. Accedere con un browser all'indirizzo <http://localhost:8000/gpi-gitlab/index.html> per vedere la presentazione

   Si pu� modificare la porta utilizzando `npm start -- --port=8001`.

### Struttura delle directories

- **template/css/** Fogli di stile legati al template (attenzione a modificare)
- **template/media/** File multimediali legati al template (attenzione a modificare)
- **presentation/** Elementi di vario genere legati al contenuto della presentazione
